let students = [];
students["student1"] = [];
students["student2"] = [];
students["student3"] = [];
students["student4"] = [];

const numOfSubjects = 4;
const numOfStudents = 4;

const javascriptCredits = 4;
const reactCredits = 7;
const pythonCredits = 6;
const javaCredits = 3;

const sumOfCredits = javascriptCredits + reactCredits + pythonCredits + javaCredits;

//student 1
students["student1"]["name"] = "Jean";
students["student1"]["lastName"] = "Reno";
students["student1"]["age"] = 26;
students["student1"]["scores"] = [];
students["student1"]["stats"] = [];

students["student1"]["scores"]["javascript"] = 62;
students["student1"]["scores"]["react"] = 57;
students["student1"]["scores"]["python"] = 88;
students["student1"]["scores"]["java"] = 90;

const student1Javascript = students["student1"]["scores"]["javascript"];
const student1React = students["student1"]["scores"]["react"];
const student1Python = students["student1"]["scores"]["python"];
const student1Java = students["student1"]["scores"]["java"];

//student 2
students["student2"]["name"] = "Claude";
students["student2"]["lastName"] = "Monet";
students["student2"]["age"] = 19;
students["student2"]["scores"] = [];
students["student2"]["stats"] = [];

students["student2"]["scores"]["javascript"] = 77;
students["student2"]["scores"]["react"] = 52;
students["student2"]["scores"]["python"] = 92;
students["student2"]["scores"]["java"] = 67;

const student2Javascript = students["student2"]["scores"]["javascript"];
const student2React = students["student2"]["scores"]["react"];
const student2Python = students["student2"]["scores"]["python"];
const student2Java = students["student2"]["scores"]["java"];

//student 3
students["student3"]["name"] = "Van";
students["student3"]["lastName"] = "Gogh";
students["student3"]["age"] = 21;
students["student3"]["scores"] = [];
students["student3"]["stats"] = [];

students["student3"]["scores"]["javascript"] = 51;
students["student3"]["scores"]["react"] = 98;
students["student3"]["scores"]["python"] = 65;
students["student3"]["scores"]["java"] = 70;

const student3Javascript = students["student3"]["scores"]["javascript"];
const student3React = students["student3"]["scores"]["react"];
const student3Python = students["student3"]["scores"]["python"];
const student3Java = students["student3"]["scores"]["java"];

//student 4
students["student4"]["name"] = "Dam";
students["student4"]["lastName"] = "Square";
students["student4"]["age"] = 36;
students["student4"]["scores"] = [];
students["student4"]["stats"] = [];

students["student4"]["scores"]["javascript"] = 82;
students["student4"]["scores"]["react"] = 53;
students["student4"]["scores"]["python"] = 80;
students["student4"]["scores"]["java"] = 65;

const student4Javascript = students["student4"]["scores"]["javascript"];
const student4React = students["student4"]["scores"]["react"];
const student4Python = students["student4"]["scores"]["python"];
const student4Java = students["student4"]["scores"]["java"];

/****************************************************************/
console.log("\n");

//Total Points for each student

const student1Total = student1Javascript + student1React + student1Python + student1Java;
students["student1"]["stats"]["totalScore"] = student1Total;
console.log(`Jean Reno Total Points: ${student1Total}`);

const student2Total = student2Javascript + student2React + student2Python + student2Java;
students["student2"]["stats"]["totalScore"] = student2Total;
console.log(`Claude Monet Total Points: ${student2Total}`);

const student3Total = student3Javascript + student3React + student3Python + student3Java;
students["student3"]["stats"]["totalScore"] = student3Total;
console.log(`Van Gogh Total Points: ${student3Total}`);

const student4Total = student4Javascript + student4React + student4Python + student4Java;
students["student4"]["stats"]["totalScore"] = student4Total;
console.log(`Dam Square Total Points: ${student4Total}`);
console.log("\n");

//Total Average for each student

const student1Average = student1Total / numOfSubjects;
students["student1"]["stats"]["AvgScore"] = student1Average;
console.log(`Jean Reno Average Points: ${student1Average}`);

const student2Average = student2Total / numOfSubjects;
students["student2"]["stats"]["AvgScore"] = student2Average;
console.log(`Claude Monet Average Points: ${student2Average}`);

const student3Average = student3Total / numOfSubjects;
students["student3"]["stats"]["AvgScore"] = student3Average;
console.log(`Van Gogh Average Points: ${student3Average}`);

const student4Average = student4Total / numOfSubjects;
students["student4"]["stats"]["AvgScore"] = student4Average;
console.log(`Dam Square Average Points: ${student4Average}`);
console.log("\n");

//GPA

//GPA student 1
const student1JavascriptGPA = 1;
const student1ReactGPA = 0.5;
const student1PythonGPA = 3;
const student1JavaGPA = 3;

const student1JavascriptAvgGPA = student1JavascriptGPA * javascriptCredits;
const student1ReactAvgGPA = student1ReactGPA * reactCredits;
const student1PythonAvgGPA = student1PythonGPA * pythonCredits;
const student1JavaAvgGPA = student1JavaGPA * javaCredits;

const student1GPA = (student1JavascriptAvgGPA + student1ReactAvgGPA + student1PythonAvgGPA + student1JavaAvgGPA) / sumOfCredits;
students["student1"]["stats"]["GPA"] = student1GPA;
console.log(`Jean Reno GPA: ${student1GPA}`);

//GPA student 2
const student2JavascriptGPA = 2;
const student2ReactGPA = 0.5;
const student2PythonGPA = 4;
const student2JavaGPA = 1;

const student2JavascriptAvgGPA = student2JavascriptGPA * javascriptCredits;
const student2ReactAvgGPA = student2ReactGPA * reactCredits;
const student2PythonAvgGPA = student2PythonGPA * pythonCredits;
const student2JavaAvgGPA = student2JavaGPA * javaCredits;

const student2GPA = (student2JavascriptAvgGPA + student2ReactAvgGPA + student2PythonAvgGPA + student2JavaAvgGPA) / sumOfCredits;
students["student2"]["stats"]["GPA"] = student2GPA;
console.log(`Claude Monet GPA: ${student2GPA}`);

//GPA student 3
const student3JavascriptGPA = 0.5;
const student3ReactGPA = 4;
const student3PythonGPA = 1;
const student3JavaGPA = 1;

const student3JavascriptAvgGPA = student3JavascriptGPA * javascriptCredits;
const student3ReactAvgGPA = student3ReactGPA * reactCredits;
const student3PythonAvgGPA = student3PythonGPA * pythonCredits;
const student3JavaAvgGPA = student3JavaGPA * javaCredits;

const student3GPA = (student3JavascriptAvgGPA + student3ReactAvgGPA + student3PythonAvgGPA + student3JavaAvgGPA) / sumOfCredits;
students["student3"]["stats"]["GPA"] = student3GPA;
console.log(`Van Gogh GPA: ${student3GPA}`);

//GPA student 4
const student4JavascriptGPA = 3;
const student4ReactGPA = 0.5;
const student4PythonGPA = 2;
const student4JavaGPA = 1;

const student4JavascriptAvgGPA = student4JavascriptGPA * javascriptCredits;
const student4ReactAvgGPA = student4ReactGPA * reactCredits;
const student4PythonAvgGPA = student4PythonGPA * pythonCredits;
const student4JavaAvgGPA = student4JavaGPA * javaCredits;

const student4GPA = (student4JavascriptAvgGPA + student4ReactAvgGPA + student4PythonAvgGPA + student4JavaAvgGPA) / sumOfCredits;
students["student4"]["stats"]["GPA"] = student4GPA;
console.log(`Dam Square GPA: ${student4GPA}`);
console.log("\n");

//Students Total Average

const studentsTotalAvg = (student1Average + student2Average + student3Average + student4Average) / numOfStudents;
console.log(`Average Total Points for students: ${studentsTotalAvg}`);
console.log("\n");

//Students Status

let student1Status = (student1Average >= studentsTotalAvg) ? "Red Diploma" : "Enemy of the People";
students["student1"]["stats"]["status"] = student1Status;
console.log(`Status for Jean Reno: ${student1Status}`);

let student2Status = (student2Average > studentsTotalAvg) ? "Red Diploma" : "Enemy of the People" ;
students["student2"]["stats"]["status"] = student2Status;
console.log(`Status for Claude Monet: ${student2Status}`);

let student3Status = (student3Average > studentsTotalAvg) ? "Red Diploma" : "Enemy of the People" ;
students["student3"]["stats"]["status"] = student3Status;
console.log(`Status for Van Gogh: ${student3Status}`);

let student4Status = (student4Average > studentsTotalAvg) ? "Red Diploma" : "Enemy of the People" ;
students["student4"]["stats"]["status"] = student4Status;
console.log(`Status for Dam Square: ${student4Status}`);
console.log("\n");

//Highest GPA

let bestByGPA = "";

if (
  student1GPA > student2GPA &&
  student1GPA > student3GPA &&
  student1GPA > student4GPA
) {
  bestByGPA = "student1";
} else if (
  student2GPA > student1GPA &&
  student2GPA > student3GPA &&
  student2GPA > student4GPA
) {
  bestByGPA = "student2";
} else if (
  student3GPA > student2GPA &&
  student3GPA > student1GPA &&
  student3GPA > student4GPA
) {
  bestByGPA = "student3";
} else if (
  student4GPA > student2GPA &&
  student4GPA > student3GPA &&
  student4GPA > student1GPA
) {
  bestByGPA = "student4";
}

console.log(`Student with highest GPA: ${students[bestByGPA]["name"]} ${students[bestByGPA]["lastName"]}`);
console.log("\n");

//Best student with average points (21+)

let bestByAvgPoints21 = "";
let maxValue = 0;

let over21 = false; //ამ ცვლადით ვამოწმებ იმ შემთხვევას, თუ არცერთი სტუდენტი არ არის 21+

if((students["student1"]["age"] >= 21) && (student1Average > maxValue)) {
    maxValue = student1Average;
    bestByAvgPoints21 = 'Jean Reno';
    over21 = true;
}
if((students["student2"]["age"] >= 21) && (student2Average > maxValue)){
    maxValue = student2Average;
    bestByAvgPoints21 = 'Claude Monet';
    over21 = true;
}
if((students["student3"]["age"] >= 21) && (student3Average > maxValue)) {
    maxValue = student3Average;
    bestByAvgPoints21 = 'Van Gogh';
    over21 = true;
}
if((students["student4"]["age"] >= 21) && (student4Average > maxValue)) {
    maxValue = student4Average;
    bestByAvgPoints21 = 'Dam Square';
    over21 = true;
}
if (!over21) {
    bestByAvgPoints21 = 'No one is 21+';
}

console.log(`Student with highest average points (21+): ${bestByAvgPoints21}`);
console.log("\n");

//best student in front-end subjects with average points

let bestInFrontEnd = "";
const numOfFrontSubjects = 2;

const student1FrontAvg =
  (student1Javascript + student1React) / numOfFrontSubjects;
const student2FrontAvg =
  (student2Javascript + student2React) / numOfFrontSubjects;
const student3FrontAvg =
  (student3Javascript + student3React) / numOfFrontSubjects;
const student4FrontAvg =
  (student4Javascript + student4React) / numOfFrontSubjects;

if (
  (student1FrontAvg > student2FrontAvg) &&
  (student1FrontAvg > student3FrontAvg) &&
  (student1FrontAvg > student4FrontAvg)
) {
  bestInFrontEnd = "student1";
} else if (
  (student2FrontAvg > student1FrontAvg) &&
  (student2FrontAvg > student3FrontAvg) &&
  (student2FrontAvg > student4FrontAvg)
) {
  bestInFrontEnd = "student2";
} else if (
  (student3FrontAvg > student1FrontAvg) &&
  (student3FrontAvg > student2FrontAvg) &&
  (student3FrontAvg > student4FrontAvg)
) {
  bestInFrontEnd = "student3";
} else if (
  (student4FrontAvg > student1FrontAvg) &&
  (student4FrontAvg > student2FrontAvg) &&
  (student4FrontAvg > student3FrontAvg)
) {
  bestInFrontEnd = "student4";
}

console.log(`Student with highest average points in Front-end subjects: ${students[bestInFrontEnd]["name"]} ${students[bestInFrontEnd]["lastName"]}`);
console.log("\n");

// console.log(students);